<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255',
            'password' => 'required|string',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|string|min:10',
            'role' => 'required|string|max:1'
        ]);

        // if($validator->fails()){
        //     return response()->json($validator->errors());       
        // }

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address, // Nullable
            'role' => $request->role,
            'key_login' => $request->key_login, // Nullable
            'created_by' => $request->created_by,
         ]);

        // $token = $user->createToken('auth_token')->plainTextToken;

        return response()
            ->json(['data' => $user]);

        // return response()
        //     ->json(['data' => $user,'access_token' => $token, 'token_type' => 'Bearer', ]);
    }

    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('username', 'password')))
        {
            return response()
                ->json(['message' => 'Unauthorized'], 401);
        }

        $user = User::where('username', $request->username)->firstOrFail();

        // $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json(['status'=>200,'message'=>'OK','name'=>$user->name, 'email'=>$user->email, 'phone'=>$user->phone]);

        // return response()
        //     ->json(['message' => 'Hi '.$user->name.', welcome to home','access_token' => $token, 'token_type' => 'Bearer', ]);
    }

    //logout // delete token
    public function logout()
    {
        // auth()->user()->tokens()->delete();
        Auth::logout();

        return [
            'message' => 'Anda telah berhasil logout'
        ];
    }
}
