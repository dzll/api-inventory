<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resources extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_user',
        'code',
        'name',
        'qty',
        'unit',
        'brand',
        'desc',
        'created_by',
    ];
}
