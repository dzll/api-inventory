<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Resource;
use App\Models\Resources;
use App\Models\Transaction;
use App\Models\ResourcesResource;
use App\Models\TransactionResource;

class TransactionController extends Controller
{
    public function getdata_resources()
    {
        $data = Resources::all();
        return $data->toArray();
    }

    public function getdata_transaction()
    {
        $data = Transaction::all();
        return $data->toArray();
    }

    public function tambah_stok(Request $request)
    {
        $get_qty_resource = Resources::where('code', $request->code)->first();

        if($get_qty_resource->code >= 0)
        {
            $resources = Resources::where('code', $request->code)->update(array(
                'qty' => $request->qty + $get_qty_resource->qty
            ));

        } else {
            $resources = Resources::create([
                'id_user' => $request->id_user,
                'code' => $request->code,
                'name' => $request->name_resources,
                'qty' => $request->qty,
                'unit' => $request->unit,
                'brand' => $request->brand,
                'desc' => $request->desc, // NUllable
                'created_by' => $request->created_by,
            ]);
    
            $res = Resource::create([
                'id_user' => $request->id_user,
                'qty' => $request->qty,
                'created_by' => $request->created_by,
            ]);

            $transaction = Transaction::create([
                'id_user' => $request->id_user,
                'name' => $request->name_transaction,
                'trans_date' => $request->trans_date,
                'type' => $request->type,
                'created_by' => $request->created_by,
            ]);
    
            $resources_res = ResourcesResource::create([
                'id_resources' => $resources->id,
                'id_res' => $res->id,
            ]);
    
            $trans_res = TransactionResource::create([
                'id_transaction' => $transaction->id,
                'id_res' => $res->id,
            ]);
        }

        return response()->json(['message' =>'Data Berhasil ditambahkan']);
    }

    public function kurangi_stok(Request $request)
    {
        $get_qty_resource = Resources::where('code', $request->code)->first();

        if($get_qty_resource->code >= 0)
        {
            $resources = Resources::where('code', $request->code)->update(array(
                'qty' => $get_qty_resource->qty - $request->qty
            ));

            $transaction = Transaction::create([
                'id_user' => $request->id_user,
                'name' => $request->name,
                'trans_date' => $request->trans_date,
                'type' => $request->type,
                'created_by' => $request->created_by,
            ]);
        }

        return response()->json(['message' => 'Success', 
                                'code' => $request->code, 
                                'qty' => $get_qty_resource->qty - $request->qty,
                                'brand' => $get_qty_resource->brand,
                                'unit' => $get_qty_resource->unit
                            ]);
    }
}
