<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Resource;
use App\Models\Resources;
use App\Models\Transaction;
use App\Models\ResourcesResource;
use App\Models\TransactionResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });



//register
Route::post('/register', [App\Http\Controllers\API\AuthController::class, 'register']);
//login
Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);
//logout
Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);

//Transaksi Stok
Route::post('/tambah_stok', [App\Http\Controllers\API\TransactionController::class, 'tambah_stok']);
Route::post('/kurangi_stok', [App\Http\Controllers\API\TransactionController::class, 'kurangi_stok']);
//View Data
Route::get('/view_resources', [App\Http\Controllers\API\TransactionController::class, 'getdata_resources']);
Route::get('/view_transaction', [App\Http\Controllers\API\TransactionController::class, 'getdata_transaction']);



//Protecting Routes
// Route::group(['middleware' => ['auth:sanctum']], function () {

//     //Transaksi Stok
//     Route::post('/tambah_stok', [App\Http\Controllers\API\TransactionController::class, 'tambah_stok']);
//     Route::post('/kurangi_stok', [App\Http\Controllers\API\TransactionController::class, 'kurangi_stok']);

//     //View Detail Stok
//     Route::post('/view_stok', [App\Http\Controllers\API\TransactionController::class, 'view_stok']);

//     //logout user
//     Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);
// });