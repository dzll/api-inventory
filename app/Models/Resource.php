<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    use HasFactory;

    protected $table = 'res';

    protected $fillable = [
        'id_user',
        'qty',
        'created_by',
    ];
}
