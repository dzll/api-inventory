<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources_resource', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('id_resources');
            $table->foreign('id_resources')->references('id')->on('resources');
            $table->unsignedBigInteger('id_res');
            $table->foreign('id_res')->references('id')->on('res');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources_resource');
    }
};
