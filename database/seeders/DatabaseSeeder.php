<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $user = new User();
        $user->name = 'Wafi';
        $user->username = 'wafi';
        $user->password = Hash::make('admin');
        $user->email = 'wafi@gmail.com';
        $user->phone = '081551000000';
        $user->role = '1';
        $user->created_by = 1;
        $user->save();

        $user = new User();
        $user->name = 'Dzul';
        $user->username = 'dzul';
        $user->password = Hash::make('reguler');
        $user->email = 'dzul@gmail.com';
        $user->phone = '081991000000';
        $user->role = '2';
        $user->created_by = 1;
        $user->save();


    }
}
