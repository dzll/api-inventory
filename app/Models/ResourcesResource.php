<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResourcesResource extends Model
{
    use HasFactory;

    protected $table = 'resources_resource';

    protected $fillable = [
        'id_resources',
        'id_res',
    ];
}
