<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionResource extends Model
{
    use HasFactory;

    protected $table = 'transaction_resource';

    protected $fillable = [
        'id_transaction',
        'id_res',
    ];
}
